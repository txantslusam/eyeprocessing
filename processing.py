import numpy as np
import cv2
import math


def process_image(image):
    height = len(image)
    width = len(image[0])
    result_image = image
    result_image = cv2.medianBlur(result_image,5)
    for x in range(1, height-1):
        for y in range(1, width-1):
            if check_nearest_neighbours(image, x, y):
                result_image[x][y] = [0,0,0]
    return result_image


def check_nearest_neighbours(image, x, y):
    is_diff = 0
    for i in range(-1,1):
        for j in range(-1,1):
            if math.fabs(image[x+i][y+j][1]-image[x][y][1]) > 30:
                is_diff = is_diff + 1
            else:
                is_diff = is_diff - 1
    if is_diff > 0:
        return True
    return False


def define_vessels(image):
    result_image = image
    transform_kernel = np.ones((5,5),np.uint8)
    result_image = cv2.erode(result_image,transform_kernel,iterations = 1)

    transform_kernel = np.ones((3,3),np.uint8)
    opening_morph = cv2.morphologyEx(result_image, cv2.MORPH_OPEN, transform_kernel)

    transform_kernel = np.ones((5,5),np.uint8)
    closing_morph = cv2.morphologyEx(opening_morph, cv2.MORPH_CLOSE, transform_kernel)

    return closing_morph

def to_binary(image):
    height = len(image)
    width = len(image[0])
    result_image = np.zeros((height,width))

    for x in range(height):
        for y in range(width):
            if np.sum(image[x][y]) > 0:
                result_image[x][y] = 255 
            else: 
                result_image[x][y] = 0

    return result_image

def compute_confusion_matrix(result_mask, mask):
    confusion_matrix = {
        "TP": 0,
        "FP": 0,
        "FN": 0,
        "TN": 0,
    }

    height = len(result_mask)
    width = len(result_mask[0])
    for x in range(height):
        for y in range(width):
            our_result = result_mask[x][y]
            true_result = mask[x][y]
            if our_result == 255 and true_result == 255:
                confusion_matrix["TP"] += 1
                continue
            
            if our_result == 255 and true_result == 0:
                confusion_matrix["FP"] += 1
                continue
                
            if our_result == 0 and true_result == 255:
                confusion_matrix["FN"] += 1
                continue
                
            if our_result == 0 and true_result == 0:
                confusion_matrix["TN"] += 1
                continue
                
    return confusion_matrix

def get_confusion_measures(confusion_matrix):
    return {
        "sensitivity": confusion_matrix["TP"]/(confusion_matrix["TP"] + confusion_matrix["FN"]),
        "specificity": confusion_matrix["TN"]/(confusion_matrix["FP"] + confusion_matrix["TN"]),
        "precision": confusion_matrix["TP"]/(confusion_matrix["TP"] + confusion_matrix["FP"]),
        "accuracy": (confusion_matrix["TP"] + confusion_matrix["TN"])/(confusion_matrix["TP"] + confusion_matrix["FN"] + confusion_matrix["TN"] + confusion_matrix["FP"]),
    }
